%% Spline Sketch MLE (SSMLE)
% NOTE that we assume alpha is known so that we can match with the CRB
% results. Change this for future.

clear all; clc; close all;


PPP = 1000; %photon count
T = 4800; % no. of narrow bins

sigma = 50;
SBR = 1;
alpha = SBR/(SBR+1);
M=8;
p1=1;
[h,~]=get_impulse(T,sigma);


knot_interval = T/M;


t0=5*knot_interval+(75);
midpoint1=5.5*knot_interval;

% load('head40m_100ms.mat')
% 
% h=h(:);
% h1=zeros(T,1);
% h1(1:length(h))=h;
% h=h1;
% [mx,idx]=max(h);
% h=circshift(h,-idx); 
% h=h/sum(h);
 h_fft=fft(h);
asym_correction = angle(h_fft(2))*T/(2*pi);


rng(12)
NMC=1000;
estim_vec=zeros(NMC,1);
estim_vec_alpha=zeros(NMC,1);
for i=1:NMC

t0 = 1800+randi(1000);
[X,~] = synthetic_lidar(h,t0,PPP,SBR);
spline_sketch1 = spline_wrapper(X,sigma,T,M,p1);
spline_sketch1.compute_sketch(X);
K=1;
estimator1=lidar_spline_estimator(spline_sketch1);
z_n=estimator1.z_n;
alpha_est = (1-M*(z_n));
alpha_est = alpha_est([1:4,7:end]);
alpha_est = mean(alpha_est);
[~,idx]=max(z_n);
% non-straddle
midpoint1=(idx-1+0.5)*knot_interval;
midpoint2=(idx+0.5)*knot_interval;

% 2 scenarios
t_est1=midpoint1 + (1/alpha)*(z_n(idx)-z_n(idx-1))*(T/(2*M))+asym_correction;
t_est2=midpoint2 + (1/alpha)*(z_n(idx+1)-z_n(idx))*(T/(2*M))+asym_correction;


% straddle
midpoint3 = idx*knot_interval;
t_est3=midpoint3 + (1/alpha)*(2)*(z_n(6)-z_n(4))*(T/(2*M))+asym_correction;


cost1=sketch_cost(z_n,alpha,t_est1,M,sigma,knot_interval,p1);
cost2=sketch_cost(z_n,alpha,t_est2,M,sigma,knot_interval,p1);
cost3=sketch_cost(z_n,alpha,t_est3,M,sigma,knot_interval,p1);

cost = [cost1,cost2,cost3];
t_est = [t_est1,t_est2,t_est3];
[~,idxc]=min(cost);
t_est_new = t_est(idxc);

estim_vec(i)=t0-t_est_new;
t0-t_est_new
estim_vec_alpha(i) = alpha-alpha_est;

end



mean(estim_vec)
sqrt(var(estim_vec))
figure
hist(estim_vec,250)

figure
hist(X,T)
hold on
for i=0:M-1
   xx=linspace((T/M)*i,(T/M)*i);
   yy=linspace(0,10);
   plot(xx,yy,'k--') 
end
xlim([0,T])
