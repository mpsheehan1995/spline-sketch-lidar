%% test_spline_lidar.m (v1), Michael P. Sheehan, 24/06/2022 
%
% "Spline Sketches: An Efficient Approach For Photon Counting Lidar"
%
% Michael P. Sheehan, Julian Tachella, Mike E. Davies
%
% Contact: michael.sheehan@ed.ac.uk    
%
%%% DESCRIPTION %%%
%
% The class spline_wrapper computes the spline sketch of size M. The class lidar_spline_estimator computes both the depth and intensity parameters
% of the scene. There are several algorithms that can be used (see the manuscript for details) including the closed form local means estimation 
% solution (linear splines), the matching pursuit algorithm and the gradient based SMLE algorithm. Use the line estimator.estim(K, alg_type, IRF_type) 
% to execute the either algorithm where 'LME', 'MP', and 'Gradient' refer to the closed form local means estimator, matching pursuit algorithm and 
% the gradient based SMLE algorithm, respectively. If the IRF is a Gaussian function then use IRF_type = 'Gaussian', otherwise if the IRF is 
% empirically computed then use IRF_type = 'Empirical' and set the IRF using estimator.h = h;. As of version 1, the 'Gradient' type can only
% handle Gaussian IRFs. Finally, the algorithm only handles splines sketch of p=0,1 and 2 where the p=0 spline sketch is equivalent to coarse
% binning. Contact michael.sheehan@ed.ac.uk for any issues.



clear all; clc; close all;



%% K=1 surface scene

fprintf('\n\n\n %%%%%%%%%%%%  Gaussian IRF %%%%%%%%%%%% \n\n\n ')


T=3000;  % clock cycle
sigma=0.001*T; % gaussian pulse width (decrease to see the bad performace of coarse binning)
sigma = 20;

t0=0.05*T+randi(round(0.9*T)); 



n=10000; % photon count
SBR=.01; % signal to background ratio
[h,~]=get_impulse(T,sigma); % get gaussian IRF
[X,~] = synthetic_lidar(h,t0,n,SBR);

[mx,idx]=max(h);
h=circshift(h,-idx+1); 
h=h/sum(h);
h_fft=fft(h);
asym_correction = angle(h_fft(2))*T/(2*pi); % asymmetric correction (for Gaussian IRF this should be 0)


%% Closed Form Solution (Piecewise Linear Splines) (currently for linear splines only however one can adapt this to quadratic splines)

fprintf('\n %%%%%%  Closed Form Linear Spline Solution (Gaussian IRF) %%%%%% \n ')

p=1; % degree of spline
M=100; % Spline sketch size (typically needs a larger sketch size to accomodate non_informative bins to calculate a good approximation of the SBR)
spline_sketch=spline_wrapper(X,sigma,T,M,p);
spline_sketch.compute_sketch(X);
K=1;
estimator=lidar_spline_estimator(spline_sketch);
[~,idx] = max(estimator.z_n);
estimator.asym_correction = asym_correction;
estimator.h=h;
estimator.estim(K,'LME','Gaussian');
disp(['(Degree p = ' num2str(p) ' Spline Sketch) ' ' True Location: ' num2str(t0) ' SMLE Estimate: ' num2str(estimator.t_est) ' Error (Bins): ' num2str(norm(t0-estimator.t_est))])


%% Matching Pursuit Algorithm (Gaussian IRF)

t_MP=zeros(3,1); alpha_MP=zeros(3,1);
fprintf('\n %%%%%%  Matching Pursuit Algorithm (Gaussian IRF) %%%%%% \n ')
for p = 0:2


spline_sketch=spline_wrapper(X,sigma,T,M,p);
spline_sketch.compute_sketch(X);
K=1;
estimator=lidar_spline_estimator(spline_sketch);
[~,idx] = max(estimator.z_n);
estimator.asym_correction = asym_correction;
estimator.t_init = mod(T/M*(idx+0.5),T); % initialize using the location maximum spline - equivalently for p=1 you can initialize with closed form solution above
estimator.alpha_init=0.5;
estimator.h=h;
estimator.estim(K,'MP','Gaussian');
t_MP(p+1)=estimator.t_est;
alpha_MP(p+1)=estimator.alpha_est;
disp(['(Degree p = ' num2str(p) ' Spline Sketch) ' ' True Location: ' num2str(t0) ' SMLE Estimate: ' num2str(estimator.t_est) ' Error (Bins): ' num2str(norm(t0-estimator.t_est))])
end


%% Gradient based SMLE algorithm (Gaussian IRF) (refine MP estimates)
fprintf(' \n %%%%%%  Gradient Based Algorithm (Gaussian IRF)  %%%%%% \n')
for p = 0:2


spline_sketch=spline_wrapper(X,sigma,T,M,p);
spline_sketch.compute_sketch(X);
K=1;
estimator=lidar_spline_estimator(spline_sketch);
[~,idx] = max(estimator.z_n);
estimator.asym_correction = asym_correction;
estimator.t_init = t_MP(p+1);
estimator.alpha_init=alpha_MP(p+1);
estimator.h=h;
estimator.estim(K,'Gradient','Gaussian');
disp(['(Degree p = ' num2str(p) ' Spline Sketch) ' ' True Location: ' num2str(t0) ' SMLE Estimate: ' num2str(estimator.t_est) ' Error (Bins): ' num2str(norm(t0-estimator.t_est))])
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Below shows how the algorithms handle a empirical IRF. Note though that currently the gradient based method can't handle empirical IRFs

fprintf('\n\n\n %%%%%%%%%%%%  Empirical Asymmetric IRF %%%%%%%%%%%% \n\n\n ')

%% Closed Form Solution (Piecewise Linear Splines) (currently for linear splines only however one can adapt this to quadratic splines)
fprintf('\n %%%%%%  Closed Form Linear Spline Solution (Empirical IRF) %%%%%% \n ')


load('head40m_100ms.mat')
dimensions=size(Y);
T=dimensions(3);

%% Aligning IRF
h=h(:);
h1=zeros(T,1);
h1(1:length(h))=h;
h=h1;
[mx,idx]=max(h);
h=circshift(h,-idx+1); 
h=h/sum(h);
h_fft=fft(h);
asym_correction = angle(h_fft(2))*T/(2*pi); 

t0=round(0.05*T)+randi(round(0.9*T)); 

%% Synthesis Empirical IRF data
n=1000;
SBR=10;
M=16;
[X,~] = synthetic_lidar(h,t0,n,SBR);

p=1;
spline_sketch=spline_wrapper(X,sigma,T,M,p);
spline_sketch.compute_sketch(X);
K=1;
estimator=lidar_spline_estimator(spline_sketch);
[~,idx] = max(estimator.z_n);
estimator.asym_correction = asym_correction;
estimator.h=h;
estimator.estim(K,'LME','Empirical');
disp(['(Degree p = ' num2str(p) ' Spline Sketch) ' ' True Location: ' num2str(t0) ' SMLE Estimate: ' num2str(estimator.t_est) ' Error (Bins): ' num2str(norm(t0-estimator.t_est))])


%% Matching Pursuit Algorithm (Empirical IRF)

t_MP=zeros(3,1); alpha_MP=zeros(3,1);
fprintf('\n %%%%%%  Matching Pursuit Algorithm (Empirical IRF)  %%%%%% \n ')
for p = 0:2


spline_sketch=spline_wrapper(X,sigma,T,M,p);
spline_sketch.compute_sketch(X);
K=1;
estimator=lidar_spline_estimator(spline_sketch);
[~,idx] = max(estimator.z_n);
estimator.asym_correction = asym_correction;
estimator.t_init = T/M*(idx+0.5); 
estimator.alpha_init=0.5;
estimator.h=h;
estimator.estim(K,'MP','Empirical');
t_MP(p+1)=estimator.t_est;
alpha_MP(p+1)=estimator.alpha_est;
disp(['(Degree p = ' num2str(p) ' Spline Sketch) ' ' True Location: ' num2str(t0) ' SMLE Estimate: ' num2str(estimator.t_est) ' Error (Bins): ' num2str(norm(t0-estimator.t_est))])
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




fprintf('\n\n\n %%%%%%%%%%%%  K=2 Surfaces %%%%%%%%%%%% \n\n\n ')
%% K=2 surface scene
T=1000;
sigma=0.01*T;
t0=[50+randi(round(0.4*T)),550+randi(round(0.4*T))];  % synthesise 2 separated peaks
 

n=1000;
SBR=10;
[h,~]=get_impulse(T,sigma);
[X,~] = synthetic_lidar(h,t0,n,SBR);

[mx,idx]=max(h);
h=circshift(h,-idx+1); 
h=h/sum(h);
h_fft=fft(h);
asym_correction = angle(h_fft(2))*T/(2*pi); % for Gaussian IRF this should be 0


M=16; % larger sketch required as more parameters to estimate

%% Matching Pursuit Algorithm (Gaussian IRF)

fprintf('\n %%%%%%  Matching Pursuit Algorithm (Gaussian IRF) %%%%%% \n ')
t_MP=zeros(3,2); alpha_MP=zeros(3,3);
for p = 0:2


spline_sketch=spline_wrapper(X,sigma,T,M,p);
spline_sketch.compute_sketch(X);
K=2;
estimator=lidar_spline_estimator(spline_sketch);
[~,idx] = max(estimator.z_n);
estimator.asym_correction = asym_correction;
estimator.t_init = mod(T/M*(idx+0.5),T); % initialize using the maximum - equivalently for p=1 you can initialize with closed form solution above
estimator.alpha_init=[0.33,0.33,0.5];
estimator.h=h;
estimator.estim(K,'MP','Gaussian');
t_MP(p+1,:) = estimator.t_est;
alpha_MP(p+1,:) = [estimator.alpha_est;1-sum(estimator.alpha_est)];
disp(['(Degree p = ' num2str(p) ' Spline Sketch) ' ' True Location: ' num2str(sort(t0)) ' SMLE Estimate: ' num2str(sort(estimator.t_est)') ' Error (Bins): ' num2str(norm(sort(t0)-sort(estimator.t_est')))])
end


%% Gradient based SMLE algorithm (Gaussian IRF) (refine MP estimates)

fprintf(' \n %%%%%%  Gradient Based Algorithm (Gaussian IRF)  %%%%%% \n')
for p = 0:2


spline_sketch=spline_wrapper(X,sigma,T,M,p);
spline_sketch.compute_sketch(X);
K=2;
estimator=lidar_spline_estimator(spline_sketch);
[~,idx] = max(estimator.z_n);
estimator.asym_correction = asym_correction;
estimator.t_init = t_MP(p+1,:);
estimator.alpha_init=alpha_MP(p+1,:);
estimator.h=h;
estimator.estim(K,'Gradient','Gaussian');
disp(['(Degree p = ' num2str(p) ' Spline Sketch) ' ' True Location: ' num2str(sort(t0)) ' SMLE Estimate: ' num2str(sort(estimator.t_est)) ' Error (Bins): ' num2str(norm(sort(t0)-sort(estimator.t_est)))])
end








