function y = SplineFunction(x,p)

% a function that computes the cardinal B-spline basis function of degree
% p. Note that the value x has been scaled and translated (about the
% circle) before input such that the range of x \in [0,p+1].


if p==0  % piecwise constant
y = x<1 & x>= 0;
elseif p==1 % piecewise linear
y = (x).*(x<1 & x>= 0) ...
+ (2-x).*(x<2 & x>= 1);
elseif p==2 % piecewise quadratic
y = (0.5*(x).^2).*(x<1 & x>= 0) ...
+ (0.5+(x-1)-(x-1).^2 ).*(x<2 & x>=1) ...
+ (0.5-(x-2)+0.5*(x-2).^2 ).*(x<3 & x>= 2) ;
else
warning("Degree of Spline must be p<=2") % larger degree future work
end
end