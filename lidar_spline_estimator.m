classdef lidar_spline_estimator<handle

properties

%output
t_est;
alpha_est;

%input
h;
asym_correction; %correction for non-symmetric bias
sigma;
sketch_obj;
M;
w;
z_n;
T;
n;
K;
p; %degree of spline
%defaults
tol;
t_init;
alpha_init;
t_step;
alpha_step;
max_iter;
init_type;
init_range;
ITER;
h_step;
loss;
LUT;

end

methods
    
    

function self=lidar_spline_estimator(sketch_obj)
self.tol=5e-1;
self.max_iter=50;
self.sketch_obj=sketch_obj;
self.M=self.sketch_obj.M;
self.T=self.sketch_obj.T;
self.n=self.sketch_obj.n;
self.t_step=1000;
self.alpha_step=1e-2;
self.z_n=self.sketch_obj.z_n;
self.init_range=0;
self.p = self.sketch_obj.p;
self.sigma=self.sketch_obj.sigma;
self.h_step = self.T/self.M;
end







function estim(self,K,alg_type,IRF_type)
self.K=K;
if strcmp(alg_type,'Gradient')
     if strcmp(IRF_type,'Gaussian')  
if self.K==1
[self.t_est,self.alpha_est]=self.sketched_lidar();
elseif self.K==2
[self.t_est,self.alpha_est]=self.sketched_lidar2D();
end
     else 
         warning('Currently the gradient based SMLE algorithm only handles Gaussian IRF functions')
     end

elseif strcmp(alg_type,'MP')
  [self.t_est,self.alpha_est]=self.matching_pursuit(IRF_type);  
    
elseif strcmp(alg_type,'LME')
[self.t_est,self.alpha_est]=self.closed_form_estim(IRF_type);  

end
end


function [t_est,alpha_est]=closed_form_estim(self,IRF_type)
if self.M<4
    warning('M>3 to recover an estimate of alpha')
end
[~,idx]=max(self.z_n);
alpha_cand = (1-self.M*(self.z_n));
non_inform = [1:idx-2,idx+2:self.M];
if idx==1
    non_inform = [3:self.M-1];
end
if idx==self.M
    non_inform = [2:self.M-2];
end
alpha_cand = alpha_cand(non_inform);
alpha_est = mean(alpha_cand);
[~,idx]=max(self.z_n);
% 3 midpoints
midpoint1=mod((idx-0.5)*self.h_step,self.T);
midpoint2=mod((idx+0.5)*self.h_step,self.T);
midpoint3 = mod(idx*self.h_step,self.T);

% 3 scenarios
t_est1=midpoint1 + (1/alpha_est)*(self.z_n(1+mod(idx-1,self.M))-self.z_n(1+mod(idx-2,self.M)))*(self.T/(2*self.M))+self.asym_correction;
t_est2=midpoint2 + (1/alpha_est)*(self.z_n(1+mod(idx,self.M))-self.z_n(1+mod(idx-1,self.M)))*(self.T/(2*self.M))+self.asym_correction;
t_est3=midpoint3 + (1/alpha_est)*(self.z_n(1+mod(idx,self.M))-self.z_n(1+mod(idx-2,self.M)))*(self.T/(self.M))+self.asym_correction;

if strcmp(IRF_type,'Gaussian')
    z1 = self.exp_spline(alpha_est,t_est1); cost1 = norm(z1-self.z_n);
    z2 = self.exp_spline(alpha_est,t_est2); cost2 = norm(z2-self.z_n);
    z3 = self.exp_spline(alpha_est,t_est3); cost3 = norm(z3-self.z_n);
    cost = [cost1,cost2,cost3];
    t_est = [t_est1,t_est2,t_est3];
    [~,idxc]=min(cost);
    t_est = t_est(idxc);
    
elseif strcmp(IRF_type,'Empirical')
    %z1 = self.spline_IRF_LUT(alpha_est,t_est1);
    z1 = alpha_est*self.LUT{round(t_est1)} + (1-alpha_est)/self.M;
    cost1 = norm(z1-self.z_n);
    
    %z2 = self.spline_IRF_LUT(alpha_est,t_est2);
    z2 = alpha_est*self.LUT{round(t_est2)} + (1-alpha_est)/self.M;
    cost2 = norm(z2-self.z_n);
    
   % z3 = self.spline_IRF_LUT(alpha_est,t_est3); 
   z3 = alpha_est*self.LUT{round(t_est3)} + (1-alpha_est)/self.M;
    cost3 = norm(z3-self.z_n);
    
    cost = [cost1,cost2,cost3];
    t_est = [t_est1,t_est2,t_est3];
    [~,idxc]=min(cost);
    t_est = t_est(idxc);
else
    warning("Incorrect IRF_type")
end

end

function [t_est,alpha_est] = matching_pursuit(self,IRF_type)
  t_est=zeros(self.K,1); alpha_est=zeros(self.K,1);
  e=self.z_n;
  expected_sketch = 0;
  for i=1:self.K
     t_loss = -Inf;
     t_start = 0;
     grid_t = mod(max(1,self.t_init- 1.5*(self.T/self.M)):min(self.T,self.t_init+1.5*(self.T/self.M)),self.T); %increase this to a wider range if needed
    % grid_t = max(self.t_init-50,0):min(self.t_init+50,self.T);
   %grid_t = 0:1:self.T;
   
     if self.K==2
         grid_t = round(0.1*self.T):round(0.8*self.T);
     end
        for k=1:length(grid_t)
        t_curr = grid_t(k);
        if strcmp(IRF_type,'Gaussian')
          z_theta = self.exp_spline(1,t_curr);
          
            
        else
        %z_theta = self.spline_IRF_LUT(self.alpha_init,t_curr);
        z_theta = self.alpha_init*self.LUT{round(t_curr)} + (1-self.alpha_init)/self.M;
        end
        
        loss_tmp = ((z_theta'/norm(z_theta))*e);
        %loss_vec(k) = loss_tmp;
        if loss_tmp>t_loss
            t_loss=loss_tmp; t_start = t_curr;
        end
        end
        t_est(i)=t_start;
        if strcmp(IRF_type,'Gaussian')
            pursuit = self.exp_spline(1,t_start);
            expected_sketch = expected_sketch + pursuit;
            alpha_est(i)=(pursuit'*self.z_n)/(pursuit'*pursuit);
            e = max(e - pursuit,0);
        else
           
        %pursuit = self.spline_IRF_LUT(self.alpha_init,t_start);
        
        pursuit = self.alpha_init*self.LUT{round(t_start)} + (1-self.alpha_init)/self.M;
       
        
        expected_sketch = expected_sketch + pursuit;
        alpha_est(i)=(pursuit'*self.z_n)/(pursuit'*pursuit)*self.alpha_init(i);
        e = max(e - pursuit,0);
        end
        
  end
 
end

function z_exp = spline_IRF_LUT(self,alpha,t0)
    t0=round(t0);
    h_tmp=circshift(self.h,t0);
    
    x=1:self.T;
    kappa = self.T/self.M;
    z_exp = zeros(self.M,1);
    for i=0:self.M-1
        spline = SplineFunction(mod(x/kappa-i,self.M),self.p)';
        if size(spline)~=size(h_tmp)
            h_tmp=h_tmp';
        end 
        area_conv = trapz(x,h_tmp.*spline);  % trapezium rule is the cheapeast and easiest integration approximation (seek other technique if needed)
        z_exp(i+1) = alpha*area_conv+(1-alpha)/self.M;
    end
end

function [t_est,alpha_est]=sketched_lidar(self)

 t_tmp=self.t_init'; alpha_tmp=self.alpha_init';
z1=self.exp_spline(alpha_tmp,t_tmp);

stop=0;it=0;
while stop==0
  it=it+1;
  G_t=self.SMLE_grad(t_tmp,alpha_tmp,z1,1,'loc');
  t0_loss=Inf;
  for lin=1:10
  t_curr=t_tmp - (0.4)^(lin-1)*self.t_step*G_t;
  z_curr=self.exp_spline(alpha_tmp,t_curr);
  Sig=eye(self.M);
  Sig = self.CF_cov(alpha_tmp,t_curr);
  while cond(Sig)>10e4
      Sig = Sig + 0.001*eye(size(Sig));
  end
  t0_loss_tmp=real((z_curr-self.z_n)'*(Sig\(z_curr-self.z_n)));
  if t0_loss_tmp<t0_loss
     t0_loss=t0_loss_tmp;
     t_new=t_curr;
     z1=z_curr;
  end
  end
  
  
 
  G_alpha=self.SMLE_grad(t_new,alpha_tmp,z1,1,'prop');
  alpha_new=alpha_tmp - self.alpha_step*G_alpha;
 z1=self.exp_spline(alpha_new,t_new);
 self.loss = norm(self.z_n-z1);
 mu_diff=norm(t_tmp-t_new);
 t_tmp=t_new;
 alpha_tmp=(alpha_new);
    
 if it>=self.max_iter
      stop=1;
 end
 error_tmp=mu_diff;
  if (error_tmp<=self.tol)
      
      stop=1;
  end
end


t_est=(t_new); alpha_est=alpha_new;
for ll=1:self.K
t_est(ll)=rem(t_est(ll),self.T);
if t_est(ll)<0
    t_est(ll)=t_est(ll)+self.T;
end
end


% alpha_est(end)=max(0,1-sum(alpha_est(1:self.K)));
% alpha_est=alpha_est/sum(alpha_est);

  %normalise to sum to 1
self.ITER=it;
end

function [t_est,alpha_est]=sketched_lidar2D(self)

t_tmp=self.t_init; alpha_tmp=self.alpha_init;
z1=self.exp_spline(alpha_tmp,t_tmp);



  


stop=0;it=0;
while stop==0
  it=it+1;
  G_t1=self.SMLE_grad(t_tmp,alpha_tmp,z1,1,'loc');
  G_t2=self.SMLE_grad(t_tmp,alpha_tmp,z1,2,'loc');
  G=[G_t1,G_t2];
  t0_loss=Inf;
  for lin=1:10
  t_curr=t_tmp - (0.4)^(lin-1)*self.t_step*G;
  z_curr=self.exp_spline(alpha_tmp,t_curr);
  Sig=eye(self.M);
  t0_loss_tmp=real((z_curr-self.z_n)'*(Sig\(z_curr-self.z_n)));
  if t0_loss_tmp<t0_loss
     t0_loss=t0_loss_tmp;
     t_new=t_curr;
     z1=z_curr;
  end
  end



  G_alpha1=self.SMLE_grad(t_new,alpha_tmp,z1,1,'prop');
  G_alpha2=self.SMLE_grad(t_new,alpha_tmp,z1,2,'prop');
  G_alpha=[G_alpha1,G_alpha2,0];
  alpha_new=alpha_tmp-self.alpha_step*G_alpha;
  z_curr=self.exp_spline(alpha_new,t_new);
  
  
    
  
  
  
  for ll=1:2
 if alpha_new(ll)<0
     alpha_new(ll)=0;
 end
 if alpha_new(ll)>=0.99
     alpha_new(ll)=0.99;
 end
  end

 
 z1=self.exp_spline(alpha_new,t_new);
 self.loss = norm(self.z_n-z1);
 mu_diff=norm(t_tmp-t_new);
 alpha_diff=norm(alpha_tmp-alpha_new);
 t_tmp=t_new;
 alpha_tmp=(alpha_new);
    
 if it>=self.max_iter
      stop=1;
 end
 error_tmp=mu_diff+alpha_diff;
  if (error_tmp<=self.tol)
      stop=1;
  end
  
 
end


t_est=(t_new);
for ll=1:2
t_est(ll)=rem(t_est(ll),self.T);
if t_est(ll)<0
    t_est(ll)=t_est(ll)+self.T;
end
end

alpha_new(3)=max(0,1-sum(alpha_new(1:2)));
alpha_est=alpha_new/sum(alpha_new);  %normalise to sum to 1
self.ITER=it;
end

function sp = exp_spline(self,alpha,t0)
    
    if length(t0)~=self.K
       alpha=[alpha,0];
       t0= [t0,0];  % solves slight class issue for K=2
    end
    
    
  sp = zeros(self.M,1);
 if self.p==0
    for k=1:self.K
    for i=1:self.M
        mu_i=self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),0);
        mu_i=alpha(k)*mu_i+(1-alpha(end))/(self.K*self.M);
        sp(i) = sp(i)+mu_i;
    end
    end
    
  elseif self.p==1
    for k=1:self.K
    for i=1:self.M
       
        mu_i = (1/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),1) - (i-1)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),0) - (1/self.h_step)*self.intGaussian(i*self.h_step,(i+1)*self.h_step,t0(k),1)+(2+i-1)*self.intGaussian(i*self.h_step,(i+1)*self.h_step,t0(k),0);
        mu_i = alpha(k)*mu_i+(1-alpha(end))/(self.K*self.M);
       if i==self.M
          mu_i = (1/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),1) - (i-1)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),0) - (1/self.h_step)*self.intGaussian(0,(1)*self.h_step,t0(k),1)+self.intGaussian(0,(1)*self.h_step,t0(k),0);
        mu_i = alpha(k)*mu_i+(1-alpha(end))/(self.K*self.M);
       end
        sp(i) = sp(i)+mu_i;
    end
    end
elseif self.p==2
   for k=1:self.K
    for i=1:self.M
           a1 = (1/(2*self.h_step^2))*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),2)-((i-1)/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),1)+(((i-1)^2)/2)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),0);
         a2 = (-1/self.h_step^2)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t0(k),2) + ((2*(i-1)+3)/self.h_step)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t0(k),1)-((2*(i-1)^2+6*(i-1)+3)/2)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t0(k),0);
         a3 = (1/(2*self.h_step^2))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t0(k),2) - (((i-1)+3)/self.h_step)*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t0(k),1)+0.5*((i-1+3)^2)*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t0(k),0);
        mu_i = alpha(k)*(a1+a2+a3)+(1-alpha(end))/(self.K*self.M);
        
         if i==self.M-1
          a1 = (1/(2*self.h_step^2))*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),2)-((i-1)/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),1)+(((i-1)^2)/2)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),0);
         a2 = (-1/self.h_step^2)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t0(k),2) + ((2*(i-1)+3)/self.h_step)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t0(k),1)-((2*(i-1)^2+6*(i-1)+3)/2)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t0(k),0);
         a3 = (1/(2*self.h_step^2))*self.intGaussian(0,(1)*self.h_step,t0(k),2) - ((1)/self.h_step)*self.intGaussian(0,(1)*self.h_step,t0(k),1)+0.5*((1)^2)*self.intGaussian(0,(1)*self.h_step,t0(k),0);
        mu_i = alpha(k)*(a1+a2+a3)+(1-alpha(end))/(self.K*self.M);
         end
         if i==self.M
         a1 = (1/(2*self.h_step^2))*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),2)-((i-1)/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),1)+(((i-1)^2)/2)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0(k),0);
         a2 = (-1/self.h_step^2)*self.intGaussian(0,(1)*self.h_step,t0(k),2) + ((3)/self.h_step)*self.intGaussian(0,(1)*self.h_step,t0(k),1)-(1/2)*self.intGaussian(0,(1)*self.h_step,t0(k),0);
         a3 = (1/(2*self.h_step^2))*self.intGaussian((1)*self.h_step,(2)*self.h_step,t0(k),2) - (2/self.h_step)*self.intGaussian((1)*self.h_step,(2)*self.h_step,t0(k),1)+0.5*((2)^2)*self.intGaussian((1)*self.h_step,(2)*self.h_step,t0(k),0);
        mu_i = alpha(k)*(a1+a2+a3)+(1-alpha(end))/(self.K*self.M);
        
         end
        
        
        
        sp(i) = sp(i)+mu_i;
       
        
    end
   end
 else
    warning("Degree of spline must be poly<3")
end




end 





function G = SMLE_grad(self,t0,alpha,z,pos,grad_type) 
t0=t0(pos);alpha=alpha(pos);
g=zeros(self.M,1);

if self.p==0
    
    Sig = self.CF_cov(alpha,t0);
    while cond(Sig)>1e4
    Sig=Sig+0.01*eye(self.M);  %added for stability
    end
 if strcmp(grad_type,'loc')
   
        for k=1:self.M
            tmp= self.diff_intGaussian_t0((k-1)*self.h_step,k*self.h_step,t0,0);
            g(k) = alpha*tmp;
        end


    elseif strcmp(grad_type,'prop')
        for i=1:self.M
            tmp = (1/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0,0)';
            g(i) = tmp;
        end


    else
        print('error')
 end
    
 
 
elseif self.p==1

Sig = self.CF_cov(alpha,t0);

while cond(Sig)>1e4
    Sig=Sig+0.01*eye(self.M);  %added for stability
end


    if strcmp(grad_type,'loc')
   
        for k=1:self.M
            tmp= (1/self.h_step)*self.diff_intGaussian_t0((k-1)*self.h_step,k*self.h_step,t0,1) - (k-1)*self.diff_intGaussian_t0((k-1)*self.h_step,k*self.h_step,t0,0) - (1/self.h_step)*self.diff_intGaussian_t0(k*self.h_step,(k+1)*self.h_step,t0,1)+(2+k-1)*self.diff_intGaussian_t0(k*self.h_step,(k+1)*self.h_step,t0,0);
            g(k) = alpha*tmp;
        end


    elseif strcmp(grad_type,'prop')
        for i=1:self.M
            tmp = (1/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0,1) - (i-1)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0,0) - (1/self.h_step)*self.intGaussian(i*self.h_step,(i+1)*self.h_step,t0,1)+(2+i-1)*self.intGaussian(i*self.h_step,(i+1)*self.h_step,t0,0);
            g(i) = tmp;
        end


    else
        print('error')
    end
elseif self.p==2
    Sig = self.CF_cov(alpha,t0);
  
    while cond(Sig)>1e4
    Sig=Sig+0.01*eye(self.M);  %added for stability
    end
    if strcmp(grad_type,'loc')
        for i=1:self.M
             a1 = (1/(2*self.h_step^2))*self.diff_intGaussian_t0((i-1)*self.h_step,i*self.h_step,t0,2)-((i-1)/self.h_step)*self.diff_intGaussian_t0((i-1)*self.h_step,i*self.h_step,t0,1)+(((i-1)^2)/2)*self.diff_intGaussian_t0((i-1)*self.h_step,i*self.h_step,t0,0);
             a2 = (-1/self.h_step^2)*self.diff_intGaussian_t0((i)*self.h_step,(i+1)*self.h_step,t0,2) + ((2*(i-1)+3)/self.h_step)*self.diff_intGaussian_t0((i)*self.h_step,(i+1)*self.h_step,t0,1)-((2*(i-1)^2+6*(i-1)+3)/2)*self.diff_intGaussian_t0((i)*self.h_step,(i+1)*self.h_step,t0,0);
             a3 = (1/(2*self.h_step^2))*self.diff_intGaussian_t0((i+1)*self.h_step,(i+2)*self.h_step,t0,2) - (((i-1)+3)/self.h_step)*self.diff_intGaussian_t0((i+1)*self.h_step,(i+2)*self.h_step,t0,1)+0.5*((i-1+3)^2)*self.diff_intGaussian_t0((i+1)*self.h_step,(i+2)*self.h_step,t0,0);
             g(i) = alpha*(a1+a2+a3);
        end    


    elseif strcmp(grad_type,'prop')
        for i=1:self.M
             a1 = (1/(2*self.h_step^2))*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0,2)-((i-1)/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0,1)+(((i-1)^2)/2)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t0,0);
             a2 = (-1/self.h_step^2)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t0,2) + ((2*(i-1)+3)/self.h_step)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t0,1)-((2*(i-1)^2+6*(i-1)+3)/2)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t0,0);
             a3 = (1/(2*self.h_step^2))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t0,2) - (((i-1)+3)/self.h_step)*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t0,1)+0.5*((i-1+3)^2)*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t0,0);
             tmp = (a1+a2+a3);%-(1/self.M);
            g(i) = tmp;
        end

    else
        print('error')
    end
    
else
    warning('Degree of spline must be p<3')
end
G=real(2*g'*(Sig\(z-self.z_n)));
end


function Sigma = CF_cov(self,alpha,t)

    Sigma=zeros(self.M,self.M);


Sig = zeros(self.M,self.M);
if self.p==0
mu_vec = self.exp_spline(alpha,t);
for i=1:self.M
    mu_i=mu_vec(i);
    for j=i:self.M
        mu_j=mu_vec(j);
        if i==j
        Sig(i,j) = mu_i - mu_i^2;
        else 
            Sig(i,j) = - mu_i*mu_j;
            Sig(j,i)= conj(- mu_i*mu_j);
        end
            
    end
end
   
elseif self.p==1
    mu_vec = self.exp_spline(alpha,t);
for i=1:self.M
    mu_i=mu_vec(i);
    for j=i:self.M
        mu_j=mu_vec(j);
        if i==j
          tmp1 = (1/self.h_step^2)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t,2) -(2*(i-1)/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t,1) + (i-1)^2*self.intGaussian((i-1)*self.h_step,i*self.h_step,t,0);
          tmp1 = tmp1 + (1/self.h_step^2)*self.intGaussian(i*self.h_step,(i+1)*self.h_step,t,2)-(2*(2+i-1)/self.h_step)*self.intGaussian(i*self.h_step,(i+1)*self.h_step,t,1)+(2+i-1)^2*self.intGaussian(i*self.h_step,(i+1)*self.h_step,t,0);   
          tmp1 = alpha*tmp1; %signal part of E(x^2)
          tmp2 = ((1-alpha)/self.T)*(2*self.h_step/3); % noise part of E(x^2)
          tmp = tmp1+tmp2; % noise and signal of E(x^2)
          Sig(i,j) = tmp - mu_i^2;
        elseif i==j-1
            tmp1 = (-1/self.h_step^2)*(self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,2)) +((2*(i-1)+3)/self.h_step)*(self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,1)) - (i*(i+1))*(self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,0));
            tmp1 = alpha*tmp1; %signal part of E(x^2)
            tmp2 = ((1-alpha)/self.T)*(self.h_step/6); % noise part of E(x^2)
            tmp = tmp1 + tmp2; % noise and signal of E(x^2)
            Sig(i,j) = tmp - mu_i*mu_j;
            Sig(j,i) = conj(tmp - mu_i*mu_j);
            
        else
            Sig(i,j) = - mu_i*mu_j;
            Sig(j,i) = conj(- mu_i*mu_j);
        end
    end
end

elseif self.p==2
    mu_vec = self.exp_spline(alpha,t);
for i=1:self.M
 mu_i = mu_vec(i); 
    for j=i:self.M
        mu_j=mu_vec(j);
       
        if i==j
            a1 = (1/(4*self.h_step^4))*self.intGaussian((i-1)*self.h_step,i*self.h_step,t,4)-(((i-1))/self.h_step^3)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t,3)+((1.5*(i-1)^2)/self.h_step^2)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t,2) - (((i-1)^3)/self.h_step)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t,1)+(((i-1)^4)/4)*self.intGaussian((i-1)*self.h_step,i*self.h_step,t,0);
            a1 = alpha*a1 +((1-alpha)/(20*self.M));
            
            b1 = (1/self.h_step^4)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,4)- ((4*(i-1)+6)/self.h_step^3)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,3) + ((6*(i-1)^2+18*(i-1)+12)/self.h_step^2)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,2) - ((4*(i-1)^3+18*(i-1)^2+24*(i-1)+9)/self.h_step)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,1)+((i-1)^4+6*(i-1)^3+12*(i-1)^2+9*(i-1)+9/4)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,0);
            b1 = alpha*b1 + (9*(1-alpha)/(20*self.M));
            
            c1 = (1/(4*self.h_step^4))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,4) - ((i-1+3)/self.h_step^3)*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,3)+ (( 3*(i-1)^2+18*(i-1)+27)/(2*self.h_step^2))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,2)- (((i-1)^3+9*(i-1)^2+27*(i-1)+27)/self.h_step)*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,1)+(((i-1)^4+12*(i-1)^3+54*(i-1)^2+108*(i-1)+81)/4)*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,0);
            c1 = alpha*c1 + ((1-alpha)/(20*self.M));
            
            d1 = a1+b1+c1;
            
            
            Sig(i,j) = d1 - mu_i*mu_j;
        elseif i==j-1
           
            
            b1 = (-1/(2*self.h_step^4))*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,4) + ((4*(i-1)+5)/(2*self.h_step^3))*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,3) - ((12*(i-1)^2+30*(i-1)+17)/(4*self.h_step^2))*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,2) + ((4*(i-1)^3+15*(i-1)^2+17*(i-1)+6)/(2*self.h_step))*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,1)-((2*(i-1)^4+10*(i-1)^3+17*(i-1)^2+12*(i-1)+3)/4)*self.intGaussian((i)*self.h_step,(i+1)*self.h_step,t,0);
            b1 = alpha*b1 + (13*(1-alpha)/(120*self.M));
            
            c1 = (-1/(2*self.h_step^4))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,4) + ((4*(i-1)+11)/(2*self.h_step^3))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,3) - ((12*(i-1)^2+66*(i-1)+89)/(4*self.h_step^2))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,2)+ ((4*(i-1)^3+33*(i-1)^2+89*(i-1)+78)/(2*self.h_step))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,1) - ((2*(i-1)^4+22*(i-1)^3+89*(i-1)^2+156*(i-1)+99)/4)*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,0);
            c1 = alpha*c1 + (13*(1-alpha)/(120*self.M));
            
            d1 = b1+c1;
            
            
            Sig(i,j) = d1 - mu_i*mu_j;
            Sig(j,i) = d1 - mu_i*mu_j;
            
        elseif i==j-2
  
            
            c1 = (1/(4*self.h_step^4))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,4) - ((2*(i-1)+5)/((2*self.h_step^3)))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,3) + ((6*(i-1)^2+30*(i-1)+37)/(4*self.h_step^2))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,2)- ((2*(i-1)^3+15*(i-1)^2+37*(i-1)+30)/(2*self.h_step))*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,1) + (((i-1)^4+10*(i-1)^3+37*(i-1)^2+60*(i-1)+36)/4)*self.intGaussian((i+1)*self.h_step,(i+2)*self.h_step,t,0);
            c1 = alpha*c1 + ((1-alpha)/(120*self.M));
            
            d1 = c1;
            
            Sig(i,j) = d1 - mu_i*mu_j;
            Sig(j,i) = d1 - mu_i*mu_j;
        else
            Sig(i,j) = - mu_i*mu_j;
            Sig(j,i) = - mu_i*mu_j;
        end
    end
end
else
    warning("wrong degree spline")
end
end

function [t0_init,alpha_init]=param_init(self,init_type)

if strcmp(init_type,'interval')
% quite rudimentary parameter initialisiation 
interval=self.T/self.K;
t0_init=zeros(self.K,1);
alpha_init=1;

for k=1:self.K
[~,idx]=max(self.z_n);

grid_t0=linspace(self.h_step*(idx-1),self.h_step*(idx+1),50);

t0_loss=Inf;
 for j=1:length(grid_t0)
  t0_tmp=grid_t0(j);
  z=self.exp_spline(1,t0_tmp);
  t0_loss_tmp=real((z-self.z_n)'*(z-self.z_n));
  if t0_loss_tmp<t0_loss
     t0_loss=t0_loss_tmp;
     t0_init(k)=t0_tmp;
  end
 end
end

end
end






function int = intGaussian(self,lower,upper,t0,poly)

% e.g. upper = (k+1)h, lower=kh



if poly==0
    int = normcdf(upper,t0,self.sigma) - normcdf(lower,t0,self.sigma);
elseif poly==1
    int = t0*(normcdf(upper,t0,self.sigma) - normcdf(lower,t0,self.sigma)) - self.sigma^2*(normpdf(upper,t0,self.sigma) - normpdf(lower,t0,self.sigma));    
elseif poly==2
   int = ((self.sigma^2+t0^2)*normcdf(upper,t0,self.sigma)-self.sigma^2*(t0+upper)*normpdf(upper,t0,self.sigma)) - ((self.sigma^2+t0^2)*normcdf(lower,t0,self.sigma)-self.sigma^2*(t0+lower)*normpdf(lower,t0,self.sigma)) ;
elseif poly==3
    int = (t0*(3*self.sigma^2+t0^2)*normcdf(upper,t0,self.sigma)-self.sigma^2*(2*self.sigma^2+t0^2+t0*upper+upper^2)*normpdf(upper,t0,self.sigma)) - (t0*(3*self.sigma^2+t0^2)*normcdf(lower,t0,self.sigma)-self.sigma^2*(2*self.sigma^2+t0^2+t0*lower+lower^2)*normpdf(lower,t0,self.sigma));
elseif poly==4
    int = ((3*self.sigma^4+t0^4+6*self.sigma^2*t0^2)*normcdf(upper,t0,self.sigma)-self.sigma^2*(t0^3+t0^2*upper+5*self.sigma^2*t0+t0*upper^2+upper^3+3*self.sigma^2*upper)*normpdf(upper,t0,self.sigma)) - ((3*self.sigma^4+t0^4+6*self.sigma^2*t0^2)*normcdf(lower,t0,self.sigma)-self.sigma^2*(t0^3+t0^2*lower+5*self.sigma^2*t0+t0*lower^2+lower^3+3*self.sigma^2*lower)*normpdf(lower,t0,self.sigma));
else
    warning('gaussian moment only up to poly<=4')
end
    end

function diff = diff_intGaussian_t0(self,lower,upper,t0,poly)

% e.g. upper = (k+1)h, lower=kh

if poly==0
    diff = -normpdf(upper,t0,self.sigma) + normpdf(lower,t0,self.sigma);
elseif poly==1
    diff = (normcdf(upper,t0,self.sigma)-upper*normpdf(upper,t0,self.sigma)) - (normcdf(lower,t0,self.sigma)-lower*normpdf(lower,t0,self.sigma));
elseif poly==2
    diff = (2*t0*normcdf(upper,t0,self.sigma)- (2*self.sigma^2+upper^2)*normpdf(upper,t0,self.sigma)) - (2*t0*normcdf(lower,t0,self.sigma)- (2*self.sigma^2+lower^2)*normpdf(lower,t0,self.sigma));
elseif poly==3
    diff = (3*(self.sigma^2+t0^2)*normcdf(upper,t0,self.sigma)-(3*self.sigma^2*t0+upper^3+3*self.sigma^2*upper)*normpdf(upper,t0,self.sigma)) - (3*(self.sigma^2+t0^2)*normcdf(lower,t0,self.sigma)-(3*self.sigma^2*t0+lower^3+3*self.sigma^2*lower)*normpdf(lower,t0,self.sigma));
elseif poly==4
    diff = (4*t0*(3*self.sigma^2+t0^2)*normcdf(upper,t0,self.sigma) - (4*self.sigma^2*(2*self.sigma^2+t0^2)+4*self.sigma^2*t0*upper+upper^4+4*self.sigma^2*upper^2)*normpdf(upper,t0,self.sigma)) - (4*t0*(3*self.sigma^2+t0^2)*normcdf(lower,t0,self.sigma) - (4*self.sigma^2*(2*self.sigma^2+t0^2)+4*self.sigma^2*t0*lower+lower^4+4*self.sigma^2*lower^2)*normpdf(lower,t0,self.sigma))  ; 
else
    warning('gaussian moment only up to poly<=4')
end
    



end






end
end