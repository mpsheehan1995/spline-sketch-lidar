classdef spline_wrapper<matlab.mixin.Copyable

properties


% sizes
M; % number of basis function (knots)
h_step; % interval size
T; % acq. time window 
n; % photon count
% frequencies
p; % degree of spline
z_n; % empirical sketch
sigma; % pulse width - use gaussian IRF for the time being

end 



methods



function self=spline_wrapper(X,sigma,T,M,p)
            if nargin>0
                
                % options
                self.T=T;
                self.n=length(X);
                self.M=M;
                self.p=p;
                self.sigma=sigma;
                self.h_step=T/M;
               
                
            end
   end

   
   




   
   
   
function compute_sketch(self,X)
        
B_sketch=zeros(self.M,1); 
for i=0:self.M-1
xx=mod(X/self.h_step-i,self.M);% use this for wrapped interval
B_sketch(i+1)=sum(SplineFunction(xx,self.p))/length(X);
end

self.z_n = B_sketch;
end




end
end


