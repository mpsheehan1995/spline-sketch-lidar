%% Spline_Lidar (v1), Michael P. Sheehan, 24/06/2022 
%
% "Spline Sketches: An Efficient Approach For Photon Counting Lidar"
%
% Michael P. Sheehan, Julian Tachella, Mike E. Davies
%
% Contact: michael.sheehan@ed.ac.uk    
%
%%% DESCRIPTION %%%
%
% The class spline_wrapper computes the spline sketch of size M. The class lidar_spline_estimator computes both the depth and intensity parameters
% of the scene. There are several algorithms that can be used (see the manuscript for details) including the closed form local means estimation 
% solution (linear splines), the matching pursuit algorithm and the gradient based SMLE algorithm. Use the line estimator.estim(K, alg_type, IRF_type) 
% to execute the either algorithm where 'LME', 'MP', and 'Gradient' refer to the closed form local means estimator, matching pursuit algorithm and 
% the gradient based SMLE algorithm, respectively. If the IRF is a Gaussian function then use IRF_type = 'Gaussian', otherwise if the IRF is 
% empirically computed then use IRF_type = 'Empirical' and set the IRF using estimator.h = h;. As of version 1, the 'Gradient' type can only
% handle Gaussian IRFs. Finally, the algorithm only handles splines sketch of p=0,1 and 2 where the p=0 spline sketch is equivalent to coarse
% binning. Contact michael.sheehan@ed.ac.uk for any issues.
